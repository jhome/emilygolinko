# Emily Golinko

Workflow: (VVV + WordPress(if you have it!)) + Gulp

Please use Vagrant site wizard to create a folder and then git clone outside the WordPress VVV Vagrant folder and then move the folder (with the .git file) to the new location. 

Make sure the contents of the htdocs is placed within your instance of the website, so htdocs/website or website/htdocs. You can grab or delete anything outside of the htdocs, it is up to you, as they are just pre-compiled tools. 

*To upload to a different location, dump existing placeholder WP database, import database and use these for the local or production url:*

`
UPDATE wp_options SET option_value = replace(option_value, 'http://emilygolinko.dev', 'http://emilygolinko.com') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET guid = replace(guid,'http://emilygolinko.dev','http://emilygolinko.com');
UPDATE wp_posts SET post_content = replace(post_content, 'http://emilygolinko.dev', 'http://emilygolinko.com');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://emilygolinko.dev','http://emilygolinko.com');
`

Place the contents of the 
## Tasks that need to be completed

- Organise archive viewing
- Organise blank 404 page showing last blog posts and top level links
- Truncate posts on archive
- Delete "Posted in" on content and archive
- Think about alternative post navigation (sidebar, pagination?)
- Add blog page title to blog archive
- Think about introduction text on welcome page, keep as part of regular page post?
- Provide content.php for both posters and illustrations

