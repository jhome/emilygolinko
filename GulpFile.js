// # main

var gulp = require('gulp');

// # plugins

var sass =  require('gulp-ruby-sass'),
    notify = require('gulp-notify'),
    header = require('gulp-header'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload

// # declare location variables

// ## Sass
var srcDir = 'src/';
var scssDir = srcDir + 'scss/';


// ## Theme for WordPress
var projectName = "emilygolinko";
var themeName = '_' + projectName;
var themeDir = 'themes/' + themeName + '/';

// ### WP Metadata
var banner = [
'/*',
' * Theme Name: Emily Golinko',
' * Theme URI: http://jameshome.co/themes/_emilygolinko',
' * Author: James Home',
' * Author URI: jameshome.co',
' * Description: Theme for Emily Golinko',
' * Author: James Home',
' * Version: 1.0',
' * License: GNU General Public License v2 or later',
' * License URI: http://www.gnu.org/licenses/gpl-2.0.html',
' * Tags: ',
' * Text Domain: emilygolinko',
' * GPL license',
' */',
''].join('\n');

// ## CSS theme location
var cssDir  = themeDir;


// # browser-sync server

gulp.task('browser-sync', function() {
  browserSync({
    proxy: projectName + '.dev'
  });
});


// # sass compile
gulp.task('sass', function() {
  // locates Sass
  return gulp.src(scssDir + '*.scss')
  // source maps none is true
  .pipe(sass({'sourcemap=none': true, style: 'expanded'}))
  // .pipe(minifyCSS({ keepSpecialComments: 1 }))
    // outputs CSS
   .pipe(gulp.dest(cssDir))
   .pipe(notify("Sass has compiled!"))
   // reloads the browser
   .pipe(reload({stream:true}));
});



gulp.task('sass-build', function() {
  // locates Sass
  return gulp.src(scssDir + '*.scss')
  // source maps none is true
  .pipe(sass({'sourcemap=none': true, style: 'expanded'}))
  // .pipe(minifyCSS({ keepSpecialComments: 1 }))
    // outputs CSS
   .pipe(gulp.dest(cssDir))
   .pipe(notify("Sass has compiled!"))
   // reloads the browser
   .pipe(reload({stream:true}));
});




// compile sass, watch directory for browser sync
gulp.task('default', ['sass','browser-sync'], function () {
  // watch sass directory
  gulp.watch(scssDir + '**/**/*.scss', ['sass']);
  gulp.watch(scssDir + '**/wp-content/themes/_emilygolinko/*.php');

});

gulp.task('build', function() {
  // locates Sass
  return gulp.src(scssDir + '*.scss')
  // source maps none is true
  .pipe(sass({'sourcemap=none': true, style: 'compressed'}))
  // .pipe(minifyCSS({ keepSpecialComments: 1 }))
    // outputs CSS
    .pipe(header(banner))
   .pipe(gulp.dest(cssDir))

   .pipe(notify("Sass has compiled!"))
   // reloads the browser
   .pipe(reload({stream:true}));
});
