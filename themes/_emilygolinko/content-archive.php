<?php
/**
 * @package _sp
 */
?>

<article id=" post-<?php the_ID(); ?>"  <?php post_class(); ?>>
	<div class="entry-content">
			<?php 
			$image = get_field('image');
			$size = 'medium';
			$thumb = $image['sizes'] [$size];
			if(!empty($image)) : ?>

	<a href="<?php echo get_permalink(); ?>"><img src="<?php echo $thumb ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', '_sp' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->