<?php
/**
 * @package _sp
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php 

$categories = get_the_category();
$category_id = $categories[0]->cat_name;
?>
	<?php if ( 'Blog' == $category_id ) : ?>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	<?php elseif ( 'Posters' == $category_id || 'Posters' == $category_id  ) : ?>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	<?php endif; ?>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', '_sp' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
