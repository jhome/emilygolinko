    (function() {
      function getWindowWidth() {
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
      }
      // Instantiate wookmark after all images have been loaded
      var wookmark;
      imagesLoaded('#image-grid', function() {
        wookmark = new Wookmark('#image-grid', {
          itemWidth: 250, // Optional min width of a grid item
          outerOffset: 10, // Optional the distance from grid to parent
          fillEmptySpace: true,
          flexibleWidth: function () {
            // Return a maximum width depending on the viewport
            return getWindowWidth() < 1024 ? '100%' : '50%';
          }
        });
      });
    })();
