<?php
/**
 * The template for displaying all single posts.
 *
 * @package _sp
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php 
			$image = get_field('image');
			if(!empty($image)) : ?>

	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	<p><?php the_field('description') ?></p>
<?php endif; ?>		
			<?php _sp_post_nav(); ?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
