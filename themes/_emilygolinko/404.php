<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package _sp
 */

get_header('empty'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="error-404 not-found">
				<header class="page-header--center">
					<h1 class="page-title"><?php _e( 'This probably isn&rsquo;t the page you were expecting.', '_sp' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
				<?php  $_SERVER['REQUEST_URI']; ?>

					<p><?php _e( 'Sorry but I couldn&rsquo;t find "'  . $_SERVER['REQUEST_URI'] . '". Please try the links below or the search feature.', '_sp' ); ?></p>

					<?php get_search_form(); ?>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
