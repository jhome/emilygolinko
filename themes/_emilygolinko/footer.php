<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _sp
 */
?>

	</div><!-- #content -->

	<footer class="site-footer" role="contentinfo">
		<div class="site-info">
<!-- 			<div class="site-icons">
				<a href="http://www.linkedin.com/profile/view?id=243200452"><img class="iconic iconic-md" data-src="<?php bloginfo('template_directory'); ?>/img/svg/smart/social.svg" data-type="linkedin"></a>
				<a href="http://www.twitter.com"><img class="iconic iconic-md" data-src="<?php bloginfo('template_directory'); ?>/img/svg/smart/social.svg" data-type="twitter"></a>
			</div> -->
			<div class="site-contact">
				<a href="mailto:hello@emilygolinko.com" class="site-email">hello@emilygolinko.com</a>
			</div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
