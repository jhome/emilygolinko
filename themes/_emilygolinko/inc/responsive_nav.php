<?php
wp_nav_menu( array(
'theme_location' => 'responsive_nav',
'container' => false,
'container_class' => '',
'menu_class' => 'nav',
'menu_id' => 'menu',
'fallback_cb' => 'wp_page_menu',
'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
) );
?>