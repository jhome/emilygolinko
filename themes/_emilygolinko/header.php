<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _sp
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<?php wp_enqueue_script("jquery"); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><b class="srt"><?php _e( 'Skip to content', '_sp' ); ?></b></a>

	<header class="site-header" role="banner">
		<div class="site-branding">
			<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<b class="srt"><?php bloginfo( 'name' ); ?></b>
			</a>
		</div><!-- .site-branding -->

		<nav class="site-nav" role="navigation">
			<a href="#menu" class="icon-toggle toggle" aria-controls="menu" aria-expanded="false"><b class="srt"><?php _e( 'Primary Menu', '_sp' ); ?></b></a>
        <?php wp_nav_menu( array(
'theme_location' => 'responsive_nav',
'container' => false,
'container_class' => '',
'menu_class' => 'nav',
'menu_id' => 'menu',
'fallback_cb' => 'wp_page_menu',
'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
