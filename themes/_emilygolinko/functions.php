<?php
/**
 * _sp functions and definitions
 *
 * @package _sp
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( '_sp_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _sp_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _sp, use a find and replace
	 * to change '_sp' to the name of your theme in all the template files
	 */
	load_theme_textdomain( '_sp', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		// 'primary' => __( 'Primary Menu', '_sp' ),
		'responsive_nav' => __( 'Responsive Menu', '_sp' )
	) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_sp_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

}
endif; // _sp_setup
add_action( 'after_setup_theme', '_sp_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function _sp_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_sp' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', '_sp_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _sp_scripts() {
	wp_enqueue_style( '_sp-style', get_stylesheet_uri() );

	// wp_enqueue_script( '_sp-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( '_imagesloaded.min.js', get_template_directory_uri() . '/js/imagesloaded.min.js', array(), false, false );
	wp_enqueue_script( '_responsive-toggle', get_template_directory_uri() . '/js/responsive-toggle.js', array(), false, true );
	wp_enqueue_script( '_iconic', get_template_directory_uri() . '/js/iconic.min.js', array(), false, true );
	wp_enqueue_script( '_iconic.min.js', get_template_directory_uri() . '/js/iconic.min.js', array(), false, false );
	wp_enqueue_script( '_sp-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	// wp_enqueue_script( '_sticky-nav.js', get_template_directory_uri() . '/js/sticky-nav.js', array(), false, true );
	wp_enqueue_script( '_wookmark.min.js', get_template_directory_uri() . '/js/wookmark.min.js', array(), false, false );
	wp_enqueue_script( '_image-grid.js', get_template_directory_uri() . '/js/image-grid.js', array(), false, true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_sp_scripts' );

function jpeg_quality_callback($arg)
{
return (int)100;
}
 
add_filter('jpeg_quality', 'jpeg_quality_callback');

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

function add_google_analytics() {
	echo '<script src="http://www.google-analytics.com/ga.js" type="text/javascript"></script>';
	echo '<script type="text/javascript">';
	echo 'var pageTracker = _gat._getTracker("UA-50166355-1");';
	echo 'pageTracker._trackPageview();';
	echo '</script>';
}
add_action('wp_footer', 'add_google_analytics');

// function sgr_show_current_cat_on_single($output) {
//      global $post;
//      if( is_single() ) {
//           $categories = wp_get_post_categories($post->ID);
//           foreach( $categories as $catid ) {
// 	  $cat = get_category($catid);

// 	       // Find cat-item-ID in the string
// 	       if(preg_match('#cat-item-' . $cat->cat_ID . '#', $output)) {
// 	            $output = str_replace('cat-item-'.$cat->cat_ID, 'cat-item-'.$cat->cat_ID . ' current-cat', $output);
// 	       }
//           }

//      }
//      return $output;
// }

// add_filter('wp_list_categories', 'sgr_show_current_cat_on_single');




function _sp_posted_on() {

}

function _sp_entry_footer() {
	
}



/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function get_cat_slug($cat_id) {
$cat_id = (int)$cat_id;
$category = &get_category($cat_id);
return $category->slug;
}
